<!DOCTYPE html>
<html lang='cs'>
    <head>
        <link rel="stylesheet" href="style.css">
        <title>TL - Služby a ceník</title>
        <meta charset='utf-8'>
        <meta name='description' content=''>
        <meta name='keywords' content=''>
        <meta name='author' content=''>
        <meta name='robots' content='all'>
        <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
        <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    </head>
    <body>
        <?php include('header.php'); ?>
        <main>
            <h1>Ceník a služby</h1>
            <div class="text">
            <p>Přemýšlíte, že si od nás necháte vyrobit nábytek na zakázku? Stačí nás kontaktovat <a href="contact.php">zde</a>.</p>
            <p>Po kontaktování nás si s Vámi domluvíme schůzku (možno přes videochat či osobně), kde probereme všechno potřebné k výrobě (materiál, velikost, doprava).</p>
            <p>Jedná-li je o kus nábytku, který potřebuje přimontovat (kuchyňská linka, práh...), rádi Vám ho osobně dovezeme a přimontujeme za rozumnou cenu.</p>
            <p>Průměrná cena nejčastěji objednaného nábytku:<p>
                <table>
                    <tr>
                        <th>Postel:</th>
                        <td>15 000 Kč</td>
                    </tr>
                    <tr>
                        <th>Odkládací stolek:</th>
                        <td>4 500 Kč</td>
                    </tr>
                    <tr>
                        <th>Židle:</th>
                        <td>1 200 Kč</td>
                    </tr>
                    <tr>
                        <th>Jídelní stůl:</th>
                        <td>8 000 Kč</td>
                    </tr>
                    <tr>
                        <th>Knihovna:</th>
                        <td>1 700 Kč</td>
                    </tr>
                    <tr>
                        <th>Polička:</th>
                        <td>500 Kč</td>
                    </tr>
                    <tr>
                        <th>Práh:</th>
                        <td>500 Kč</td>
                    </tr>
                    <tr>
                        <th>Dveře:</th>
                        <td>2 000 Kč</td>
                    </tr>
                    <tr>
                        <th>Kuchyňská linka:</th>
                        <td>12 000 Kč</td>
                    </tr>
                    <tr>
                        <th>Křeslo:</th>
                        <td>2 000 Kč</td>
                    </tr>
                </table>
            </div>   
        </main>
        <?php include('footer.php'); ?>
    </body>
</html>