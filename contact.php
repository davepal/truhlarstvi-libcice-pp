﻿<!DOCTYPE html>
<html lang='cs'>
    <head>
        <link rel="stylesheet" href="style.css">
        <title>TL - Kontakt</title>
        <meta charset='utf-8'>
        <meta name='description' content=''>
        <meta name='keywords' content=''>
        <meta name='author' content=''>
        <meta name='robots' content='all'>
        <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
        <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    </head>
    <body>
        <?php include('header.php'); ?>
        <main>
            <h1>Kontaktujte nás</h1>
            <article class="contact">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2554.1718613577686!2d14.366113315951768!3d50.19531841359612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470bc18cd7afb1ab%3A0xd60428b163227de9!2sDru%C5%BEstevn%C3%AD%20256%2C%20252%2066%20Lib%C4%8Dice%20nad%20Vltavou!5e0!3m2!1scs!2scz!4v1611001460428!5m2!1scs!2scz" 
            width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            <p>Adresa: Družstevní 265, Libčice nad Vltavou</p>
            <p>Email: truhlarstvilibcice@gmail.com</p>
            <p>Telefon: +420 722 411 253</p>
            </article>
            <br>
            <br>
            <h2 class="clearh">Máte dotaz? Napište nám!</h2>
            <div class="formular">
                <form id="dotaz">
                    <label for="jmeno">Jméno:</label>
                    <input type="text" id="jmeno" name="jméno">
                    <label for="prijmeni">Příjmení:</label>
                    <input type="text" id="prijmeni" name="příjmení"><br>
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email"><br>
                    <textarea rows="8" cols="50" name="comment" form="dotaz">Dotaz</textarea><br>
                    <input type="submit" value="Odeslat">
                </form>
            </div>
        </main>
        <?php include('footer.php'); ?>
    </body>
</html>