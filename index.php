<!DOCTYPE html>
<html lang='cs'>
    <head>
        <link rel="stylesheet" href="style.css">
        <title>TL - Úvod</title>
        <meta charset='utf-8'>
        <meta name='description' content=''>
        <meta name='keywords' content=''>
        <meta name='author' content=''>
        <meta name='robots' content='all'>
        <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
        <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    </head>
    <body>
        <?php include('header.php'); ?>
        <main>
            <h1>Vítejte na stránkách Truhlářství Libčice!</h1>
            <article class="fade">
                <img class="articleleft" src="images/galerie/nabytek1.jpg" alt="nabytek" title="Nábytek" height="500" width="500">
                <h2>Kdo jsme?</h2>
                <p>Jsme rodinná truhlářská firma současně sídlící v Libčicích nad Vltavou. Truhlařinu děláme již po několik generací a naše učení předáváme dál na mladší generace.</p>
            </article>
            <article class="fade">
                <img class="articleright" src="images/galerie/nabytek2.jpg" alt="nabytek" title="Nábytek" height="500" width="500">
                <h2>Jakou máme praxi?</h2>
                <p>Jak již bylo zmíněno, truhlařině se naše rodina věnuje po mnoho generací. Již přes pů století vyrábíme dřevený nábytek rychle a za rozumnou cenu. A to vše s láskou a s rodinnou tradicí.</p>
            </article>
            <article class="fade">
                <img class="articleleft" src="images/galerie/nabytek3.jpg" alt="nabytek" title="Nábytek" height="500" width="500">
                <h2>Proč volit právě nás?</h2>
                <p>Dalo by se říct, že umění pracovat s dřevem máme v krvi. Vše dokážeme vyřešit rychle a levně. Navíc dokážeme pracovat se všemi druhy dřeva. Od akácie po zmarliky.</p>
            </article>
        </main>
        <?php include('footer.php'); ?>
    </body>
</html>