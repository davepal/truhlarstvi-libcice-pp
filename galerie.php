﻿<!DOCTYPE html>
<html lang='cs'>
    <head>
        <link rel="stylesheet" href="style.css">
        <title>TL - Galerie</title>
        <meta charset='utf-8'>
        <meta name='description' content=''>
        <meta name='keywords' content=''>
        <meta name='author' content=''>
        <meta name='robots' content='all'>
        <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
        <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    </head>
    <body>
        <?php include('header.php'); ?>
        <main>
            <div class="gallery">
            <a target="_blank" href="images/galerie/nabytek1.jpg">
            <img src="images/galerie/nabytek1.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            <a target="_blank" href="images/galerie/nabytek2.jpg">
            <img src="images/galerie/nabytek2.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            <a target="_blank" href="images/galerie/nabytek3.jpg">
            <img src="images/galerie/nabytek3.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            <a target="_blank" href="images/galerie/nabytek4.jpg">
            <img src="images/galerie/nabytek4.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            <a target="_blank" href="images/galerie/nabytek5.jpg">
            <img src="images/galerie/nabytek5.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            <a target="_blank" href="images/galerie/nabytek6.jpg">
            <img src="images/galerie/nabytek6.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            <a target="_blank" href="images/galerie/nabytek7.jpg">
            <img src="images/galerie/nabytek7.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            <a target="_blank" href="images/galerie/nabytek8.jpg">
            <img src="images/galerie/nabytek8.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            <a target="_blank" href="images/galerie/nabytek9.jpg">
            <img src="images/galerie/nabytek9.jpg" alt="nabytek" title="Nábytek" height="400" width="400">
            </a>
            </div>
        </main>
        <?php include('footer.php'); ?>
    </body>
</html>